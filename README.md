Malayalam POS (Part of Speech) Tagged Corpus
============================================
Developed Jointly By:  ICFOSS Trivandrum and Malayalam University

In corpus linguistics, part-of-speech tagging (POS tagging or PoS tagging or POST), also called grammatical tagging or word-category disambiguation, is the process of marking up a word in a text (corpus) as corresponding to a particular part of speech, based on both its definition and its context.

There is comparatively large number of POS tagging works available for Malayalam;which differs in tag sets. The tag set that are commonly and widely used are flat tag set (eg: IIT Hyderabad tag set) and hierarchical tag set (eg: BIS tag set).Unlike flat tag sets, a hierarchical tag set exploits the linguistic hierarchy among
categories. Most hierarchical framework of tags has a hierarchy at three levels: categories, types, and attributes. Here the BIS tag set is used to tag the untagged Malayalam corpus.refer BIS tagset.pdf at the root of this source tree.


LICENSE
-------
Copyright (c) 2018 ICFOSS

The contents of this source tree is licensed for usage and distribution under the terms of MIT License.
For more details, refer LICENSE.txt file at the root of this source tree.

